import { combineReducers } from 'redux';
import { slice as commonSlice } from './modules/common';
import { configureStore } from '@reduxjs/toolkit';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { slice as formSlice } from './modules/form';
import { slice as scalesSlice } from './modules/home';

export const history = createBrowserHistory();

const rootReducer = combineReducers({
  router: connectRouter(history),
  form: formSlice.reducer,
  common: commonSlice.reducer,
  home: scalesSlice.reducer,
});

export type GlobalState = ReturnType<typeof rootReducer>;

const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware => {
    const customMiddleware = [routerMiddleware(history)];

    return getDefaultMiddleware({
      serializableCheck: false,
      immutableCheck: false,
    }).concat(customMiddleware);
  },
});

export default store;
