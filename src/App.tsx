import * as React from 'react';
import { ConnectedRouter } from 'connected-react-router';
import { Home } from './modules/home';
import { Layout } from './modules/common';
import { Provider } from 'react-redux';
import { R } from './modules/common';
import { Redirect, Route, Switch } from 'react-router-dom';
import store, { history } from './store';

function App(): React.ReactElement {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Layout>
          <Switch>
            <Route exact path={R.ROOT} component={Home}/>
            <Redirect from={'*'} to={R.ROOT}/>
          </Switch>
        </Layout>
      </ConnectedRouter>
    </Provider>
  );
}

export default App;
