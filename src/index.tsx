import './reset.css';
import * as React from 'react';
import * as registerServiceWorker from './registerServiceWorker';
import App from './App';
import ReactDOM from 'react-dom';

ReactDOM.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
  document.getElementById('root'),
);

registerServiceWorker.register();
