import { createSlice } from '@reduxjs/toolkit';

export type CommonState = {
  pendingActions: string[];
}

const slice = createSlice({
  name: 'common',
  initialState: { pendingActions: [] } as CommonState,
  reducers: {
    addPendingAction: (state: CommonState, action: { payload: string }) => {
      state.pendingActions.push(action.payload);
    },
    removePendingAction: (state: CommonState, action: { payload: string }) => {
      const { payload } = action;

      state.pendingActions.splice(state.pendingActions.indexOf(payload), 1);
    },
  },
});

export default slice;
