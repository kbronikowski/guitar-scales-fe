import * as React from 'react';

type ModalControl = [
  boolean,
  () => void,
  () => void,
]

export default function useModalState(): ModalControl {
  const [isOpen, setIsOpen] = React.useState(false);

  const onClose = React.useCallback(() => setIsOpen(false), [setIsOpen]);
  const onOpen = React.useCallback(() => setIsOpen(true), [setIsOpen]);

  return [isOpen, onOpen, onClose];
}
