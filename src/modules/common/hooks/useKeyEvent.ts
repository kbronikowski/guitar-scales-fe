import * as React from 'react';

type KeyEventType = 'keyup' | 'keydown' | 'keypress';

type Handler = (e: KeyboardEvent) => void;

type Options = {
  // Target `KeyboardEvent.key`. If empty, targets all keys.
  readonly key?: string | string[];
  // Restrict callback to ony fire when `activeElement` is the current `document.activeElement`.
  // Defaults to `document.body`.
  readonly activeElement?: HTMLElement | null;
  // Defaults to `keyup`.
  readonly eventType?: KeyEventType;
}

export default function useKeyEvent(handler: Handler, config: Options): void {
  const { key, activeElement = document.body, eventType = 'keyup' } = config;

  const shouldExecuteHandler = React.useCallback((e: KeyboardEvent): boolean => {
    if (activeElement && document.activeElement !== activeElement) {
      return false;
    }

    if (key instanceof Array && !key.includes(e.key)) {
      return false;
    }

    return !(typeof key === 'string' && key !== e.key);
  }, [key, activeElement]);

  const callback = React.useCallback((e: KeyboardEvent): void => {
    if (shouldExecuteHandler(e)) {
      handler(e);
    }
  }, [shouldExecuteHandler, handler]);

  React.useEffect(() => {
    window.addEventListener<typeof eventType>(eventType, callback);

    return (): void => window.removeEventListener<typeof eventType>(eventType, callback);
  }, [callback, eventType]);
}
