import * as React from 'react';
import classes from './variants.module.scss';
import classnames from 'classnames';
import TypographyBase, { InheritedProps } from './TypographyBase';

export const Overline = (props: InheritedProps): React.ReactElement => (
  <TypographyBase baseClass={classes.overline} element={'div'} {...props}/>
);

export const MainHeading = (props: InheritedProps): React.ReactElement => (
  <TypographyBase element={'h1'} baseClass={classes.mainHeading} {...props}/>
);

export const BodyText = (props: InheritedProps & { bold?: boolean }): React.ReactElement => {
  const rootClass = classnames(
    classes.bodyText,
    {
      [classes.bold]: props.bold,
    },
  );

  return <TypographyBase baseClass={rootClass} element={'div'} {...props}/>;
};
