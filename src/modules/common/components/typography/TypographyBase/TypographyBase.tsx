import * as React from 'react';
import classes from './TypographyBase.module.scss';
import classnames from 'classnames';

export type InheritedProps = {
  children: NonNullable<React.ReactNode>;
  className?: string;
  html?: boolean;
}

type Props = InheritedProps & {
  baseClass: string;
  element: 'div' | 'span' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
}

const TypographyBase = (props: Props): React.ReactElement => {
  const { children, baseClass, className, html } = props;
  const rootClass = classnames(classes.root, baseClass, className);

  if (html) {
    if (typeof children !== 'string') {
      throw new Error('Cannot render non-string children as html.');
    }

    return <props.element className={rootClass} dangerouslySetInnerHTML={{ __html: children }}/>;
  }

  return (
    <props.element className={rootClass}>
      {children}
    </props.element>
  );
};

export default TypographyBase;
