import { InheritedProps as InheritedPropsT } from './TypographyBase';

export { default } from './TypographyBase';
export type InheritedProps = InheritedPropsT;
