import * as React from 'react';
import classes from './Layout.module.scss';
import Navigation from '../Navigation';

type Props = {
  children: NonNullable<React.ReactNode>;
}

const Layout = (props: Props): React.ReactElement => {
  const { children } = props;

  return (
    <div className={classes.root}>
      <Navigation/>
      <div className={classes.container}>
        {children}
      </div>
    </div>
  );
};

export default Layout;
