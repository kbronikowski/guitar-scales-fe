import * as React from 'react';
import { Close } from '@material-ui/icons';
import classes from './Modal.module.scss';
import classnames from 'classnames';
import IconButton from '../../../common/components/IconButton';
import ReactDOM from 'react-dom';
import useKeyEvent from '../../hooks/useKeyEvent';

type Props = React.PropsWithChildren<{
  isOpen: boolean;
  onClose: (e?: React.MouseEvent<HTMLButtonElement>) => void;
  Controls?: React.ReactChild,
}>

const Modal = (props: Props): React.ReactElement => {
  const { isOpen, onClose, Controls, children } = props;

  const onCloseKeyboard = React.useCallback(() => onClose(), [onClose]);

  useKeyEvent(onCloseKeyboard, { key: 'Escape', activeElement: null });

  const rootClass = classnames(classes.root, { [classes.open]: isOpen });

  return ReactDOM.createPortal(
    <div className={rootClass}>
      <div className={classes.innerContainer}>
        <div className={classes.controls}>
          <IconButton onClick={onClose}>
            <Close/>
          </IconButton>
          {Controls && Controls}
        </div>
        {children}
        {/* Firefox collapses bottom padding */}
        <div className={classes.bottomPadding}/>
      </div>
    </div>,
    document.getElementById('portal') as HTMLDivElement,
  );
};

export default Modal;
