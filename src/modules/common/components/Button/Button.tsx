import * as React from 'react';
import classes from './Button.module.scss';
import classnames from 'classnames';

type Props = React.ButtonHTMLAttributes<HTMLButtonElement>

const Button = (props: Props): React.ReactElement => {
  const { children, className, ...buttonProps } = props;

  return (
    <button type={'button'} className={classnames(classes.root, className)} {...buttonProps}>
      {children}
    </button>
  );
};

export default Button;
