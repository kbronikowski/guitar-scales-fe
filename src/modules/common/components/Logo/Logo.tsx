import * as React from 'react';
import classes from './Logo.module.scss';
import classnames from 'classnames';

type Props = {
  className?: string;
  withText?: boolean;
}

const Logo = (props: Props): React.ReactElement => {
  const { className, withText } = props;
  const viewBoxX = withText ? '164' : '28';

  return (
    <div className={classnames(classes.root, className)}>
      <svg xmlns={'http://www.w3.org/2000/svg'} viewBox={`0 0 ${viewBoxX} 32`}>
        {/* eslint-disable-next-line max-len */}
        <path d={'M0.994867 16.7551C-0.849559 10.9591 3.65604 6.3439 8.41607 3.5957L8.42407 3.59107C13.1841 0.842872 19.4338 -0.7515 23.531 3.7438C28.6526 9.36293 28.1935 28.7428 24.2574 31.0153L24.2494 31.0199C20.3134 33.2923 3.3004 24 0.994867 16.7551Z'} fill={'currentColor'}/>
        <text transform={'translate(44 20)'} className={classes.text} fill={'currentColor'}>
          Guitar Scales
        </text>
      </svg>
    </div>
  );
};

export default Logo;
