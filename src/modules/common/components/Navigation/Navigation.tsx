import * as React from 'react';
// import { Menu } from '@material-ui/icons';
import classes from './Navigation.module.scss';
import classnames from 'classnames';
// import IconButton from '../IconButton';
import Logo from '../Logo';
import useIsNoDistractionsMode from '../../../home/hooks/useIsNoDistractionsMode';

const Navigation = (): React.ReactElement => {
  const isNoDistractionsMode = useIsNoDistractionsMode();

  return (
    <nav className={classnames(classes.root, { [classes.noDistractions]: isNoDistractionsMode })}>
      <Logo withText/>
      {/*<IconButton>*/}
      {/*  <Menu/>*/}
      {/*</IconButton>*/}
    </nav>
  );
};

export default Navigation;
