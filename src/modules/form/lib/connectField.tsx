import * as React from 'react';
import { GlobalState } from '../../../store';
import { msg } from '../../localization';
import { useDispatch, useSelector } from 'react-redux';
import slice from '../slice';

interface InjectedProps {
  onChange(e: React.ChangeEvent<HTMLInputElement> | boolean): void;
  value: string | boolean;
  name: string;
  label: string;
}

type Options = {
  formName: string;
  fieldName: string;
}

export default function connectField(options: Options) {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  return <P extends InjectedProps, T extends HTMLElement>(Component: React.ComponentType<P>) => {
    return React.forwardRef<T, Omit<React.ComponentProps<typeof Component>, keyof InjectedProps>>((props, ref): React.ReactElement => {
      const { formName, fieldName } = options;
      const dispatch = useDispatch();
      const value = useSelector((state: GlobalState) => state.form[formName]?.[fieldName]?.value);
      const name = `${formName}_${fieldName}`;
      const label = msg([`form.${formName}.${fieldName}.label`,`form.${fieldName}.label`]);

      React.useEffect(() => {
        dispatch(slice.actions.register({ formName, fieldName }));
      }, [dispatch, formName, fieldName]);

      const onChange = React.useCallback((e: React.ChangeEvent<HTMLInputElement> | boolean) => {
        const value = typeof e === 'boolean' ? e : e.target.value;

        dispatch(slice.actions.setValue({ formName, fieldName, value }));
      }, [dispatch, formName, fieldName]);

      return <Component {...props as P} onChange={onChange} value={value} name={name} label={label} ref={ref}/>;
    });
  };
}
