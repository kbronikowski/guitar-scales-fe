import * as React from 'react';
import { Check } from '@material-ui/icons';
import classes from './Checkbox.module.scss';
import classnames from 'classnames';

type ControlProps = {
  onChange: (value: boolean) => void;
  value: boolean;
  name: string;
  label: string;
}

type Props = ControlProps;

const Checkbox = (props: Props): React.ReactElement => {
  const { onChange: _onChange, name, value, label } = props;

  const onChange = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    _onChange(e.target.checked);
  }, [_onChange]);

  return (
    <label htmlFor={name} className={classes.root}>
      <input id={name} type={'checkbox'} onChange={onChange} checked={value || false}/>
      <div className={classnames(classes.checkbox, { [classes.checked]: value })}>
        <Check/>
      </div>
      <span>{label}</span>
    </label>
  );
};

export default Checkbox;
