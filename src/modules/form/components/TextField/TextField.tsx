import * as React from 'react';
import classes from './TextField.module.scss';
import classnames from 'classnames';

type ControlProps = {
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
  name: string;
  label: string;
}

type Props = ControlProps & {
  className?: string;
  StartIcon?: React.ComponentType;
  onAfterChange?: (value: string) => void;
}

const TextField = React.forwardRef<HTMLInputElement, Props>((props, ref): React.ReactElement => {
  const { name, value, label, onAfterChange, onChange: _onChange, StartIcon = null } = props;
  const [isFocused, setIsFocused] = React.useState(false);

  const onFocus = React.useCallback(() => setIsFocused(true), []);
  const onBlur = React.useCallback(() => setIsFocused(false), []);

  const onChange = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    _onChange(e);

    if (onAfterChange) {
      onAfterChange(e.target.value);
    }
  }, [_onChange, onAfterChange]);

  const rootClass = classnames(
    classes.root,
    props.className || '',
    {
      [classes.focused]: isFocused,
      [classes.empty]: !value,
      [classes.startIcon]: StartIcon,
    },
  );

  return (
    <div className={rootClass}>
      {StartIcon && <StartIcon/>}
      <input id={name} name={name} onFocus={onFocus} onBlur={onBlur} value={value || ''} onChange={onChange} ref={ref}/>
      <label htmlFor={name}>
        {label}
      </label>
    </div>
  );
});

export default TextField;
