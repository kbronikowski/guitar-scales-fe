import { assocPath, path } from 'ramda';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type InputState = {
  value: string | boolean;
  error: string | null;
}

type FormState = {
  [key: string]: {
    [key: string]: InputState;
  }
}

type WithCommonPayload<T> = PayloadAction<T & {
  formName: string;
  fieldName: string;
}>

type PayloadValue = ((value?: string) => string) | ((value?: boolean) => boolean) | string | boolean;

const slice = createSlice({
  name: 'form',
  initialState: {} as FormState,
  reducers: {
    register: (state: FormState, action: WithCommonPayload<any>) => {
      const { formName, fieldName } = action.payload;

      return assocPath([formName, fieldName], { value: '', error: null }, state);
    },
    setValue: (
      state: FormState,
      action: WithCommonPayload<{ value: PayloadValue }>,
    ) => {
      const { formName, fieldName, value } = action.payload;
      const nextValue = typeof value === 'function' ? value(path([formName, fieldName, 'value'], state)) : value;

      return assocPath([formName, fieldName, 'value'], nextValue, state);
    },
  },
});

export default slice;
