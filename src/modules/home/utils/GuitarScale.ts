import { ChromaticDegree, Pitch, Tuning } from '../types';
import { compose, reverse, splitEvery, transpose as transposeArray, uniq } from 'ramda';
import ValidationError from './ValidationError';

type ChromaticDegreeToPitchMap = { [key: number]: Pitch };
type PitchToChromaticDegreeMap = { [key: string]: ChromaticDegree };
type TranslationMaps = {
  chromaticDegreeToPitchMap: ChromaticDegreeToPitchMap;
  pitchToChromaticDegreeMap: PitchToChromaticDegreeMap;
}

export type PresenceMap = (ChromaticDegree | null)[][];

type GetNotePresenceOptions = {
  transpose?: boolean;
  fullOctaves?: boolean;
}

export default class GuitarScale {
  private PITCH_PROGRESSION: Pitch[] = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'];

  private definition: number[];
  private tuning: Tuning;
  private tonic: Pitch;

  private _translationMaps: TranslationMaps;

  _createChromaticDegreeMap(startFret: number, endFret: number): ChromaticDegree[][] {
    if (startFret < 0 || endFret < startFret || endFret > 23) {
      throw new ValidationError('Arguments `startFret` and `endFret` out of range (0;23).');
    }

    return reverse([...Array(6)].map((_, index) => {
      const startDegree = this.getChromaticDegreeFromPitch(this.tuning[index]);

      return [...Array(endFret - startFret)].map((_, index) => {
        return (startFret + startDegree + index) % 12;
      });
    })) as ChromaticDegree[][];
  }

  _createTranslationMaps(): TranslationMaps {
    const chromaticDegreeToPitchMap = [...Array(12)].reduce((all, _, index) => ({
      ...all,
      [index]: this.PITCH_PROGRESSION[
        (this.PITCH_PROGRESSION.indexOf(this.tonic) + index) % this.PITCH_PROGRESSION.length
      ],
    }), {});

    const pitchToChromaticDegreeMap = Object.keys(chromaticDegreeToPitchMap).reduce((all, k) => ({
      ...all,
      [chromaticDegreeToPitchMap[parseInt(k)]]: parseInt(k),
    }), {});

    return { chromaticDegreeToPitchMap, pitchToChromaticDegreeMap };
  }

  constructor(definition: ChromaticDegree[], tonic: Pitch = 'E', tuning: Tuning = ['E', 'A', 'D', 'G', 'B', 'E']) {
    this.definition = definition;
    this.tuning = tuning;
    this.tonic = tonic;
    this._translationMaps = this._createTranslationMaps();
  }

  getPitchFromChromaticDegree(degree: ChromaticDegree): Pitch {
    return this._translationMaps.chromaticDegreeToPitchMap[degree];
  }

  getChromaticDegreeFromPitch(pitch: Pitch): ChromaticDegree {
    return this._translationMaps.pitchToChromaticDegreeMap[pitch];
  }

  setTonic(tonic: Pitch): this {
    this.tonic = tonic;
    this._translationMaps = this._createTranslationMaps();

    return this;
  }

  setTuning(tuning: Tuning): this {
    this.tuning = tuning;

    return this;
  }

  static validateDefinition(definition: number[]): true {
    if (definition.some(n => n > 11 || n < 0) || uniq(definition).length !== definition.length) {
      throw new ValidationError('Scale must be defined with unique chromatic degrees within one octave (0-11).');
    }

    definition.forEach((n, index) => {
      if (index > 0 && definition[index - 1] > n) {
        throw new ValidationError('Scale `definition` must be in ascending order.');
      }
    });

    return true;
  }

  /**
   * Creates a note presence map for the current scale.
   *
   * E.g. map[1][3] === true would mean fret 3 (index 3) (0th index is the empty string)
   * on string 2 (index 1) (B in standard tuning) is present in the scale.
   *
   * Options:
   * - `transpose` - Switches indices - map[1][3] - fret[1], string[3] (D in standard tuning)
   * - `fullOctaves` - Show only full octaves i.e. removes notes before the tonic on the low E
   *   and after the tonic on the high E
   */
  getNotePresenceMap(
    startFret: number,
    endFret: number,
    options: GetNotePresenceOptions = {},
  ): PresenceMap {
    const { transpose, fullOctaves } = options;
    const range = endFret - startFret;
    const presenceMap: PresenceMap = splitEvery(range, [...Array(range * 6)].fill(null));
    let chromaticDegreeMap = this._createChromaticDegreeMap(startFret, endFret);

    if (fullOctaves) {
      const chromaticDegreeList: number[] = reverse(chromaticDegreeMap).reduce((a, b) => a.concat(b));
      chromaticDegreeList.fill(-1, 0, chromaticDegreeList.indexOf(0));
      chromaticDegreeList.fill(-1, chromaticDegreeList.lastIndexOf(0) + 1, chromaticDegreeList.length);

      chromaticDegreeMap = reverse(splitEvery(range, chromaticDegreeList)) as ChromaticDegree[][];
    }

    chromaticDegreeMap.forEach((i, index) => {
      i.forEach((j, idx) => {
        presenceMap[index][idx] = this.definition.includes(j) ? j : null;
      });
    });

    return transpose ? compose(transposeArray)(presenceMap) : presenceMap;
  }
}
