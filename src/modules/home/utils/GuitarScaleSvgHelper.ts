import { groupWith } from 'ramda';
import GuitarScale from './GuitarScale';

type PositionDefinition = [number, number]

type PositionMap = PositionDefinition[][];

export default class GuitarScaleSvgHelper {
  scale: GuitarScale;

  constructor(scale: GuitarScale) {
    this.scale = scale;
  }

  /**
   * Positions defined as vertical ranges (because they are rendered in vertical order).
   * Returns a PositionMap for the 1st position of a scale.
   *
   * E.g.
   * const map = [
   *    [
   *      [0, 2], // High E to G on fret 1
   *    ],
   *    [
   *      [0, 0], // High E on fret 2
   *      [2, 5], // G to low E on fret 2
   *    ],
   * ];
   */
  getScalePositionMap(startFret: number, endFret: number): PositionMap {
    const presenceMap = this.scale.getNotePresenceMap(startFret, endFret, { transpose: true, fullOctaves: true });

    return presenceMap.map(i => {
      const presentIndices = i.map((n, index) => n === null ? null : index).filter(j => j !== null);
      const positions = groupWith<number>((a, b) => b - a === 1, presentIndices as number[]);

      return positions.map(p => [p[0], p[p.length - 1]]);
    }) as PositionMap;
  }
}
