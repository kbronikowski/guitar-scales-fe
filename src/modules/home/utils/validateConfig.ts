import { Scale } from '../types';
import GuitarScale from './GuitarScale';
import ValidationError from './ValidationError';

export default function validateConfig(config: Omit<Scale, 'id'>[]): void {
  if (config.filter(s => s.default).length !== 1) {
    throw new ValidationError('Config must have at exactly one default scale.');
  }

  config.forEach(({ definition, name }) => {
    try {
      GuitarScale.validateDefinition(definition);
    } catch (e) {
      throw new ValidationError(`[${name}] ${e.message}`);
    }
  });
}
