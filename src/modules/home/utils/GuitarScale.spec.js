import GuitarScale from './GuitarScale';
import ValidationError from './ValidationError';

const CHROMATIC_SCALE_DEFINITION = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

describe('GuitarScale', () => {
  it('should translate pitch to chromatic degree', () => {
    const guitarScale = new GuitarScale(CHROMATIC_SCALE_DEFINITION, 'E');

    expect(guitarScale.getChromaticDegreeFromPitch('E')).toBe(0);
    expect(guitarScale.getChromaticDegreeFromPitch('F')).toBe(1);
    expect(guitarScale.getChromaticDegreeFromPitch('F#')).toBe(2);
    expect(guitarScale.getChromaticDegreeFromPitch('G')).toBe(3);
    expect(guitarScale.getChromaticDegreeFromPitch('G#')).toBe(4);
    expect(guitarScale.getChromaticDegreeFromPitch('A')).toBe(5);
    expect(guitarScale.getChromaticDegreeFromPitch('A#')).toBe(6);
    expect(guitarScale.getChromaticDegreeFromPitch('B')).toBe(7);
    expect(guitarScale.getChromaticDegreeFromPitch('C')).toBe(8);
    expect(guitarScale.getChromaticDegreeFromPitch('C#')).toBe(9);
    expect(guitarScale.getChromaticDegreeFromPitch('D')).toBe(10);
    expect(guitarScale.getChromaticDegreeFromPitch('D#')).toBe(11);

    guitarScale.setTonic('D#');

    expect(guitarScale.getChromaticDegreeFromPitch('D#')).toBe(0);
  });

  it('should translate chromatic degree to pitch', () => {
    const guitarScale = new GuitarScale(CHROMATIC_SCALE_DEFINITION, 'E');

    expect(guitarScale.getPitchFromChromaticDegree(0)).toBe('E');
    expect(guitarScale.getPitchFromChromaticDegree(1)).toBe('F');
    expect(guitarScale.getPitchFromChromaticDegree(2)).toBe('F#');
    expect(guitarScale.getPitchFromChromaticDegree(3)).toBe('G');
    expect(guitarScale.getPitchFromChromaticDegree(4)).toBe('G#');
    expect(guitarScale.getPitchFromChromaticDegree(5)).toBe('A');
    expect(guitarScale.getPitchFromChromaticDegree(6)).toBe('A#');
    expect(guitarScale.getPitchFromChromaticDegree(7)).toBe('B');
    expect(guitarScale.getPitchFromChromaticDegree(8)).toBe('C');
    expect(guitarScale.getPitchFromChromaticDegree(9)).toBe('C#');
    expect(guitarScale.getPitchFromChromaticDegree(10)).toBe('D');
    expect(guitarScale.getPitchFromChromaticDegree(11)).toBe('D#');

    guitarScale.setTonic('D#');

    expect(guitarScale.getPitchFromChromaticDegree(0)).toBe('D#');
  });

  it('should throw an error when validating an invalid scale definition', () => {
    expect(() => GuitarScale.validateDefinition([-1])).toThrow(ValidationError);
    expect(() => GuitarScale.validateDefinition([12])).toThrow(ValidationError);
    expect(() => GuitarScale.validateDefinition([1, 1])).toThrow(ValidationError);
    expect(() => GuitarScale.validateDefinition([0, 2, 1])).toThrow(ValidationError);
  });

  it('should create a chromatic presence map', () => {
    const guitarScale1 = new GuitarScale(CHROMATIC_SCALE_DEFINITION);

    expect(guitarScale1.getNotePresenceMap(0, 1)).toEqual([[0],[7],[3],[10],[5],[0]]);
    expect(guitarScale1.getNotePresenceMap(6, 8)).toEqual([[6,7],[1,2],[9,10],[4,5],[11,0],[6,7]]);
    expect(guitarScale1.getNotePresenceMap(12, 13)).toEqual([[0],[7],[3],[10],[5],[0]]);

    guitarScale1.setTonic('D#');

    expect(guitarScale1.getNotePresenceMap(0, 1)).toEqual([[1],[8],[4],[11],[6],[1]]);

    const guitarScale2 = new GuitarScale([0]);

    expect(guitarScale2.getNotePresenceMap(0, 1)).toEqual([[0],[null],[null],[null],[null],[0]]);
    expect(guitarScale2.getNotePresenceMap(12, 13)).toEqual([[0],[null],[null],[null],[null],[0]]);
  });

  it('should create a transposed chromatic presence map', () => {
    const guitarScale1 = new GuitarScale(CHROMATIC_SCALE_DEFINITION);

    expect(guitarScale1.getNotePresenceMap(0, 1, { transpose: true })).toEqual([[0, 7, 3, 10, 5, 0]]);
  });
});
