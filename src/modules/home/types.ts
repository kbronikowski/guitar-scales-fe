export type Scale = {
  id: number;
  name: string;
  title?: string;
  definition: ChromaticDegree[];
  default?: boolean;
}

export type ChromaticDegree = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11
export type Pitch = 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'A#' | 'C#' | 'D#' | 'F#' | 'G#';
export type Tuning = [Pitch, Pitch, Pitch, Pitch, Pitch, Pitch];
