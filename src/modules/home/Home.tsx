import * as React from 'react';
import classes from './Home.module.scss';
import classnames from 'classnames';
import ScaleDisplay from './components/ScaleDisplay';
import ScaleSelector from './components/ScaleSelector';
import useIsNoDistractionsMode from './hooks/useIsNoDistractionsMode';

const Home = (): React.ReactElement => {
  const isNoDistractionsMode = useIsNoDistractionsMode();

  return (
    <>
      <ScaleSelector/>
      <div className={classnames(classes.noDistractionsOverlay, { [classes.open]: isNoDistractionsMode })}/>
      <ScaleDisplay/>
    </>
  );
};

export default Home;
