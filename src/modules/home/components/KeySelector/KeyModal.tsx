import * as React from 'react';
import { Pitch } from '../../types';
import { useDispatch } from 'react-redux';
import classes from './KeyModal.module.scss';
import Modal from '../../../common/components/Modal';
import slice from '../../slice';

const ALL_PITCHES: (Pitch | '')[] = ['C', 'D', 'E', 'F', 'G', 'A', 'B', 'C#', 'D#', '', 'F#', 'G#', 'A#', ''];

type Props = {
  onClose: () => void;
  isOpen: boolean;
}

const KeyModal = (props: Props): React.ReactElement => {
  const { isOpen, onClose } = props;
  const dispatch = useDispatch();
  const handlers = React.useRef<{ [key: string]: () => void }>({});

  const createSelectHandler = React.useCallback((value: Pitch) => {
    if (handlers.current[value]) {
      return handlers.current[value];
    }

    const onSelect = () => {
      dispatch(slice.actions.setTonic(value));
      onClose();
    };

    return (handlers.current[value] = onSelect);
  }, [dispatch, handlers, onClose]);

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <div className={classes.keysOuterContainer}>
        <div className={classes.keysInnerContainer}>
          {ALL_PITCHES.map((p, index) => (
            <button
              type={'button'}
              className={!p ? classes.empty : ''}
              onClick={p ? createSelectHandler(p) : undefined}
              key={index}
            >
              {p}
            </button>
          ))}
        </div>
      </div>
    </Modal>
  );
};

export default KeyModal;
