import * as React from 'react';
import { MainHeading } from '../../../common';
import Button from '../../../common/components/Button';
import classes from './KeySelector.module.scss';
import classnames from 'classnames';
import KeyModal from './KeyModal';
import useCurrentKey from '../../hooks/useCurrentKey';
import useIsNoDistractionsMode from '../../hooks/useIsNoDistractionsMode';
import useModalState from '../../../common/hooks/useModalState';

type Props = {
  className?: string;
}

const KeySelector = (props: Props): React.ReactElement => {
  const { className } = props;
  const current = useCurrentKey();
  const [isModalOpen, onOpen, onClose] = useModalState();
  const isNoDistractionsMode = useIsNoDistractionsMode();

  return (
    <>
      <div className={classnames(classes.root, className, { [classes.noDistractions]: isNoDistractionsMode })}>
        <MainHeading>
          Key of
        </MainHeading>
        <Button className={classes.current} onClick={onOpen}>
          {current}
        </Button>
      </div>
      <KeyModal onClose={onClose} isOpen={isModalOpen}/>
    </>
  );
};

export default KeySelector;
