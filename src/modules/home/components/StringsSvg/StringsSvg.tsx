import * as React from 'react';
import Svg from '../../../common/components/Svg';

const STRING_COUNT = 6;

type Props = Omit<React.SVGAttributes<HTMLOrSVGElement>, 'viewBox'> & React.PropsWithChildren<{
  viewBoxX: number;
  viewBoxY: number;
}>

const StringsSvg = (props: Props): React.ReactElement => {
  const { viewBoxX, viewBoxY, children, ...svgProps } = props;
  const offset = viewBoxY / STRING_COUNT;
  const padding = offset / 2;

  return (
    <Svg viewBox={`0 0 ${viewBoxX} ${viewBoxY}`} {...svgProps}>
      <g>
        {[...Array(STRING_COUNT)].map((_, index) => (
          <path d={`M 0 ${index * offset + padding} H ${viewBoxX}`} key={index}/>
        ))}
      </g>
      {children}
    </Svg>
  );
};

export default React.memo(StringsSvg);
