import * as React from 'react';
import { Checkbox, connectField } from '../../../form';
import classes from './Toggles.module.scss';
import classnames from 'classnames';
import useIsNoDistractionsMode from '../../hooks/useIsNoDistractionsMode';

const ToggleNoDistractionsMode = connectField({ formName: 'toggles', fieldName: 'noDistractions' })(Checkbox);
const ToggleShowNoteNames = connectField({ formName: 'toggles', fieldName: 'noteNames' })(Checkbox);

type Props = {
  className?: string;
}

const Toggles = (props: Props): React.ReactElement => {
  const { className } = props;
  const isNoDistractionsMode = useIsNoDistractionsMode();

  return (
    <div className={classnames(classes.root, className, { [classes.noDistractions]: isNoDistractionsMode })}>
      <ToggleShowNoteNames/>
      <ToggleNoDistractionsMode/>
    </div>
  );
};

export default Toggles;
