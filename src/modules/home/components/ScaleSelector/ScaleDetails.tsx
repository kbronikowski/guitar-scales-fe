import * as React from 'react';
import { MainHeading } from '../../../common';
import classes from './ScaleDetails.module.scss';
import useScale from '../../hooks/useScale';

type Props = {
  id: number;
  onSelect: (id: number) => void;
}

const ScaleDetails = (props: Props): React.ReactElement => {
  const { id, onSelect } = props;
  const scale = useScale(id);

  const onClick = React.useCallback(() => {
    onSelect(id);
  }, [id, onSelect]);

  return (
    <button className={classes.root} onClick={onClick}>
      <MainHeading html>
        {scale.title || scale.name}
      </MainHeading>
    </button>
  );
};

export default ScaleDetails;
