import * as React from 'react';
import { slice as formSlice } from '../../../form';
import { RefObject } from 'react';
import { useDispatch } from 'react-redux';
import useKeyEvent from '../../../common/hooks/useKeyEvent';

const LOWERCASE = 'abcdefghijklmnopqrstuvwxyz';
const UPPERCASE = LOWERCASE.toUpperCase();
const LETTERS = LOWERCASE.split('').concat(UPPERCASE.split(''));

type Props = {
  searchFieldRef: RefObject<HTMLInputElement | null>;
}

const ScalesModalKeyboardEvents = (props: Props): React.ReactElement => {
  const { searchFieldRef } = props;
  const dispatch = useDispatch();

  const onLetterPress = React.useCallback((e: KeyboardEvent) => {
    const input = searchFieldRef.current;

    if (input && input !== document.activeElement) {
      input.focus();

      if (e.key !== 'Backspace') {
        dispatch(formSlice.actions.setValue({
          formName: 'search',
          fieldName: 'query',
          value: (v?: string) => `${v || ''}${e.key}`,
        }));
      }
    }
  }, [dispatch, searchFieldRef]);

  useKeyEvent(onLetterPress, { key: [...LETTERS, 'Backspace'], activeElement: null });

  // eslint-disable-next-line react/jsx-no-useless-fragment
  return <></>;
};

export default ScalesModalKeyboardEvents;
