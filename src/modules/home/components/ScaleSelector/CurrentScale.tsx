import * as React from 'react';
import { MainHeading } from '../../../common';
import { useDispatch } from 'react-redux';
import Button from '../../../common/components/Button';
import classes from './CurrentScale.module.scss';
import ScaleIconSvg from './ScaleIconSvg';
import ScalesModal from './ScalesModal';
import slice from '../../slice';
import useScale from '../../hooks/useScale';

const CurrentScale = (): React.ReactElement => {
  const scale = useScale('current');
  const dispatch = useDispatch();

  const onOpen = React.useCallback(() => {
    dispatch(slice.actions.onOpenScalesModal());
  }, [dispatch]);

  return (
    <>
      <Button className={classes.root} onClick={onOpen}>
        <ScaleIconSvg definition={scale.definition}/>
        <MainHeading html className={classes.heading}>
          {scale.title || scale.name}
        </MainHeading>
      </Button>
      <ScalesModal/>
    </>
  );
};

export default CurrentScale;
