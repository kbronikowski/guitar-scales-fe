import * as React from 'react';
import classes from './Background.module.scss';
import Svg from '../../../common/components/Svg';

const PATH_COUNT = 6;
const INNER_SPACING = 12;
const OUTER_SPACING = 30;
const RATIO = 6;
const BEZIER_THRESHOLD = 0.2;

const VIEW_BOX_Y = OUTER_SPACING * (PATH_COUNT - 1);
const VIEW_BOX_X = VIEW_BOX_Y * RATIO;
const VIEW_BOX = `0 0 ${VIEW_BOX_X} ${VIEW_BOX_Y}`;

const INNER_PADDING = (VIEW_BOX_Y - INNER_SPACING * (PATH_COUNT - 1)) / 2;

const Background = (): React.ReactElement => {
  return (
    <Svg viewBox={VIEW_BOX} className={classes.root} preserveAspectRatio={'none'}>
      {[...Array(PATH_COUNT)].map((_, index) => {
        const outerY = index * OUTER_SPACING;
        const innerY = index * INNER_SPACING + INNER_PADDING;
        const controlPointX = VIEW_BOX_X * BEZIER_THRESHOLD;
        const firstBezier = `Q ${controlPointX} ${innerY}, ${VIEW_BOX_X / 2} ${innerY}`;
        const secondBezier = `Q ${VIEW_BOX_X - controlPointX} ${innerY}, ${VIEW_BOX_X} ${outerY}`;

        return (
          <path d={`M 0 ${outerY} ${firstBezier} ${secondBezier}`} key={index}/>
        );
      })}
    </Svg>
  );
};

export default Background;
