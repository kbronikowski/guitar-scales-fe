import * as React from 'react';
import { connectField, TextField } from '../../../form';
import { GlobalState } from '../../../../store';
import { msg } from '../../../localization';
import { Search } from '@material-ui/icons';
import { useDispatch, useSelector } from 'react-redux';
import classes from './ScalesModal.module.scss';
import Modal from '../../../common/components/Modal';
import ScaleDetails from './ScaleDetails';
import ScalesModalKeyboardEvents from './ScalesModalKeyboardEvents';
import slice from '../../slice';

const SearchField = connectField({ fieldName: 'query', formName: 'search' })(TextField);

const ScalesModal = (): React.ReactElement => {
  const searchFieldRef = React.useRef<HTMLInputElement | null>(null);
  const scales = useSelector((state: GlobalState) => state.home.entities);
  const allScaleIds = useSelector((state: GlobalState) => state.home.ids);
  const dispatch = useDispatch();
  const query = useSelector<GlobalState, string | undefined>(state => state.form.search?.query?.value as string);
  const isOpen = useSelector<GlobalState, boolean>(state => state.home.isScalesModalOpen);

  const onClose = React.useCallback(() => {
    dispatch(slice.actions.onCloseScalesModal());
  }, [dispatch]);

  const onSelect = React.useCallback((id: number) => {
    dispatch(slice.actions.setCurrent(id));
    onClose();
  }, [dispatch, onClose]);

  const scaleIds = React.useMemo(() => {
    if (!query) {
      return allScaleIds;
    }

    return Object.keys(scales).filter(s => scales[s].name.toLowerCase().includes(query.toLowerCase())).map(s => scales[s].id);
  }, [query, allScaleIds, scales]);

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      Controls={<SearchField className={classes.searchField} StartIcon={Search} ref={searchFieldRef}/>}
    >
      {isOpen && <ScalesModalKeyboardEvents searchFieldRef={searchFieldRef}/>}
      <div className={classes.scalesContainer}>
        {scaleIds.map(i => <ScaleDetails id={i} onSelect={onSelect} key={i}/>)}
        {scaleIds.length === 0 && (
          <div className={classes.noResults}>
            {msg('search.no_results')}
          </div>
        )}
      </div>
    </Modal>
  );
};

export default ScalesModal;
