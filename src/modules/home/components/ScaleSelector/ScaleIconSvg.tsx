import * as React from 'react';
import { last } from 'ramda';
import { Scale } from '../../types';
import classes from './ScaleIconSvg.module.scss';
import GuitarScale from '../../utils/GuitarScale';
import GuitarScaleSvgHelper from '../../utils/GuitarScaleSvgHelper';
import StringsSvg from '../StringsSvg';

type Props = Pick<Scale, 'definition'>

const VIEW_BOX_X = 80;
const VIEW_BOX_Y = 60;
const CAP_RADIUS = 3;

const ScaleIconSvg = ({ definition }: Props): React.ReactElement => {
  const positionMap = React.useMemo(() => {
    const map = new GuitarScaleSvgHelper(new GuitarScale(definition)).getScalePositionMap(11, 16);

    if (map[0].length === 0) {
      map.shift();
    }

    if (last(map)?.length === 0) {
      map.pop();
    }

    return map;
  }, [definition]);

  return (
    <StringsSvg viewBoxX={VIEW_BOX_X} viewBoxY={VIEW_BOX_Y} className={classes.root}>
      {positionMap.map((i, index) => {
        return i.map((p, idx) => {
          const startY = p[0] * 10 + CAP_RADIUS;
          const endY = (p[1] + 1) * 10 - CAP_RADIUS;
          const gapX = VIEW_BOX_X / positionMap.length;

          return (
            <path
              d={`M ${gapX * index + 10} ${startY} V ${endY}`}
              className={classes.noteGroup}
              key={idx}
            />
          );
        });
      })}
    </StringsSvg>
  );
};

export default React.memo(ScaleIconSvg);
