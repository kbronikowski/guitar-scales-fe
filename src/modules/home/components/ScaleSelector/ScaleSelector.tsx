import * as React from 'react';
import Background from './Background';
import classes from './ScaleSelector.module.scss';
import classnames from 'classnames';
import CurrentScale from './CurrentScale';
import useIsNoDistractionsMode from '../../hooks/useIsNoDistractionsMode';

const ScaleSelector = (): React.ReactElement => {
  const isNoDistractionsMode = useIsNoDistractionsMode();

  return (
    <div className={classnames(classes.root, { [classes.noDistractions]: isNoDistractionsMode })}>
      <Background/>
      <CurrentScale/>
    </div>
  );
};

export default ScaleSelector;
