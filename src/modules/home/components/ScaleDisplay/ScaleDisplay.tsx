import * as React from 'react';
import { GlobalState } from '../../../../store';
import { MainHeading } from '../../../common';
import { ScreenRotation } from '@material-ui/icons';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../../../common/components/Button';
import classes from './ScaleDisplay.module.scss';
import classnames from 'classnames';
import KeySelector from '../KeySelector';
import slice from '../../slice';
import StringsSvg from '../StringsSvg';
import Toggles from '../Toggles';
import useCurrentGuitarScale from '../../hooks/useCurrentGuitarScale';
import useCurrentKey from '../../hooks/useCurrentKey';
import useIsNoDistractionsMode from '../../hooks/useIsNoDistractionsMode';
import useScale from '../../hooks/useScale';

const FRET_NUMBERS = [...Array(22)].map((_, index) => index + 1);

function getNoteClass(chromaticDegree: number, stringIndex: number, showNoteNames: boolean) {
  const conditionalClasses = {
    [classes.rootNote]: chromaticDegree ===  0,
    [classes.withNoteName]: showNoteNames,
  };

  return classnames(classes.note, classes[`string${stringIndex}`], conditionalClasses);
}

const ScaleDisplay = (): React.ReactElement => {
  const currentGuitarScale = useCurrentGuitarScale();
  const currentScale = useScale('current');
  const tonic = useCurrentKey();
  const showNoteNames = useSelector<GlobalState, boolean>(state => Boolean(state.form.toggles?.noteNames?.value));
  const isNoDistractionsMode = useIsNoDistractionsMode();
  const dispatch = useDispatch();

  const presenceMap = React.useMemo(() => {
    return currentGuitarScale.getNotePresenceMap(0, 23, { transpose: true });
  }, [currentGuitarScale]);

  const onOpenScalesModal = React.useCallback(() => {
    dispatch(slice.actions.onOpenScalesModal());
  }, [dispatch]);

  return (
    <div className={classes.root}>
      <div>
        <MainHeading className={classes.mobileScaleInfo} html>
          {`${currentScale.title || currentScale.name} in the key of <b>${tonic}</b>`}
        </MainHeading>
        <div className={classnames(classes.scaleName, { [classes.noDistractions]: isNoDistractionsMode })}>
          <Button onClick={onOpenScalesModal}>
            <MainHeading html>
              {currentScale.title || currentScale.name}
            </MainHeading>
          </Button>
        </div>
        <KeySelector className={classes.keySelector}/>
        <div className={classes.fretNumbers}>
          {FRET_NUMBERS.map(n => <span key={n}>{n}</span>)}
        </div>
        <div className={classes.stringsContainer}>
          <StringsSvg viewBoxX={1000} viewBoxY={144} preserveAspectRatio={'none'}/>
          <div>
            {[...Array(24)].map((_, index) => (
              <div className={classes.fret} key={index}>
                {index < 23 && presenceMap[index].map((p, idx) => p !== null ? (
                  <div className={getNoteClass(p, idx, showNoteNames)} key={idx}>
                    {currentGuitarScale.getPitchFromChromaticDegree(p)}
                  </div>
                ) : null)}
              </div>
            ))}
          </div>
        </div>
        <div className={classes.mobileDisclaimer}>
          <div>
            Rotate your screen to see the scale
            <ScreenRotation/>
            For the full experience, visit the desktop site :)
          </div>
        </div>
        <Toggles className={classes.toggles}/>
      </div>
    </div>
  );
};

export default ScaleDisplay;
