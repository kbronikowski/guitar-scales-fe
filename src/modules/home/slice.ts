import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { normalize, schema } from 'normalizr';
import { Pitch, Scale } from './types';
import scales from './scales.json';
import validateConfig from './utils/validateConfig';

validateConfig(scales as Omit<Scale, 'id'>[]);

type ScalesState = {
  current: number;
  tonic: Pitch;
  entities: { [key: string]: Scale };
  ids: number[];

  isScalesModalOpen: boolean;
}

const slice = createSlice({
  name: 'scales',
  initialState: ((): ScalesState => {
    const scalesWithId = scales.map((s, index) => ({ ...s, id: index + 1 }));
    const { entities: { scales: entities }, result: ids } = normalize(scalesWithId, [new schema.Entity('scales')]);
    const current = (scalesWithId.find(s => s.default) as Scale).id;

    return { ids, entities, current, tonic: 'C', isScalesModalOpen: false };
  })(),
  reducers: {
    setCurrent: (state: ScalesState, action: PayloadAction<number>) => {
      state.current = action.payload;
    },
    setTonic: (state: ScalesState, action: PayloadAction<Pitch>) => {
      state.tonic = action.payload;
    },
    onCloseScalesModal: (state: ScalesState) => {
      state.isScalesModalOpen = false;
    },
    onOpenScalesModal: (state: ScalesState) => {
      state.isScalesModalOpen = true;
    },
  },
});

export default slice;
