import * as React from 'react';
import GuitarScale from '../utils/GuitarScale';
import useCurrentKey from './useCurrentKey';
import useScale from './useScale';

export default function useCurrentGuitarScale(): GuitarScale {
  const scale = useScale('current');
  const key = useCurrentKey();

  return React.useMemo(() => {
    return new GuitarScale(scale.definition, key);
  }, [scale, key]);
}
