import { GlobalState } from '../../../store';
import { Pitch } from '../types';
import { useSelector } from 'react-redux';

export default function useCurrentKey(): Pitch {
  return useSelector(({ home: { tonic } }: GlobalState) => tonic);
}
