import { GlobalState } from '../../../store';
import { useSelector } from 'react-redux';

export default function useIsNoDistractionsMode(): boolean {
  return useSelector<GlobalState, boolean>(state => Boolean(state.form.toggles?.noDistractions?.value));
}
