import { GlobalState } from '../../../store';
import { Scale } from '../types';
import { useSelector } from 'react-redux';

export default function useScale(id: number | 'current'): Scale {
  return useSelector(({ home: { current, entities } }: GlobalState) => entities[typeof id === 'number' ? id : current]);
}
